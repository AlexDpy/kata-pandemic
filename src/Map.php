<?php

namespace Kata;

class Map
{
    /**
     * @var CityLink[]
     */
    private $cityLinks;

    /**
     * Map constructor.
     *
     * @param CityLink[] $cityLinks
     */
    public function __construct(array $cityLinks)
    {
        $this->cityLinks = $cityLinks;
    }

    /**
     * @param City $city
     * @return City[]
     */
    public function getNeighbours(City $city): array
    {
        $neighbourLinks = array_filter($this->cityLinks, function (CityLink $cityLink) use ($city) {
            return $cityLink->contains($city);
        });

        return array_map(function (CityLink $cityLink) use ($city) {
            return $cityLink->getOtherCity($city);
        }, $neighbourLinks);
    }

    public function eclose(Eclosion $eclosion): void
    {
        $neighbours = $this->getNeighbours($eclosion->getCity());

        foreach ($neighbours as $neighbour) {
            $neighbour->contaminate($eclosion->getVirus());
        }
    }
}
