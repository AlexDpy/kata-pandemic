<?php

namespace Kata\Listeners;

use Kata\Eclosion;
use Kata\Map;
use Kata\VirusLimitReached;
use League\Event\EventInterface;
use League\Event\ListenerInterface;

class MapEclosionListener implements ListenerInterface
{
    /**
     * @var Map
     */
    private $map;

    /**
     * EclosionListener constructor.
     *
     * @param Map $map
     */
    public function __construct(Map $map)
    {
        $this->map = $map;
    }

    /**
     * Handle an event.
     *
     * @param EventInterface $event
     *
     * @return void
     */
    public function handle(EventInterface $event): void
    {
        /** @var VirusLimitReached $event */
        $this->map->eclose(new Eclosion($event->getCity(), $event->getVirus()));
    }

    /**
     * Check whether the listener is the given parameter.
     *
     * @param mixed $listener
     *
     * @return bool
     */
    public function isListener($listener): bool
    {
        return $listener === $this;
    }
}
