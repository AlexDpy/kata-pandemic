<?php

namespace Kata\Listeners;

use Kata\EclosionCounter;
use League\Event\EventInterface;
use League\Event\ListenerInterface;

class CounterEclosionListener implements ListenerInterface
{
    /**
     * @var EclosionCounter
     */
    private $eclosionCounter;

    /**
     * EclosionListener constructor.
     *
     * @param EclosionCounter $eclosionCounter
     */
    public function __construct(EclosionCounter $eclosionCounter)
    {
        $this->eclosionCounter = $eclosionCounter;
    }

    /**
     * Handle an event.
     *
     * @param EventInterface $event
     *
     * @return void
     */
    public function handle(EventInterface $event): void
    {
        $this->eclosionCounter->increment();
    }

    /**
     * Check whether the listener is the given parameter.
     *
     * @param mixed $listener
     *
     * @return bool
     */
    public function isListener($listener): bool
    {
        return $listener === $this;
    }
}
