<?php

namespace Kata;

use League\Event\EmitterInterface;

class EclosionCounter
{
    /**
     * @var int
     */
    private $count;

    /**
     * @var EmitterInterface
     */
    private $eventEmitter;

    /**
     * EclosionCounter constructor.
     *
     * @param EmitterInterface $emitter
     */
    public function __construct(EmitterInterface $emitter)
    {
        $this->count = 0;
        $this->eventEmitter = $emitter;
    }

    public function increment(): void
    {
        $this->count++;

        $this->eventEmitter->emit(new EclosionCounterIncremented($this->count));

        if ($this->count === 8) {
            $this->eventEmitter->emit(new EclosionCounterLimitReached());
        }
    }

    public function count(): int
    {
        return $this->count;
    }
}
