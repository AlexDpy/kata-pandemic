<?php

namespace Kata;

use League\Event\EmitterInterface;

final class City
{
    /**
     * @var array
     */
    private $contaminated;

    /**
     * @var
     */
    private $name;

    /**
     * @var EmitterInterface
     */
    private $eventEmitter;

    /**
     * City constructor.
     *
     * @param string           $name
     * @param EmitterInterface $eventEmitter
     */
    public function __construct(string $name, EmitterInterface $eventEmitter)
    {
        $this->contaminated = [];
        $this->name = $name;
        $this->eventEmitter = $eventEmitter;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Virus $virus
     */
    public function contaminate(Virus $virus): void
    {
        if (!$this->hasVirus($virus)) {
            $this->contaminated[(string) $virus] = 0;
        }

        if ($this->contaminated[(string) $virus] === 3) {
            $this->eventEmitter->emit(new VirusLimitReached($this, $virus));

            return;
        }

        ++$this->contaminated[(string) $virus];
    }

    /**
     * @param Virus $virus
     * @return int
     */
    public function countContamination(Virus $virus): int
    {
        if (!$this->hasVirus($virus)) {
            return 0;
        }

        return $this->contaminated[(string) $virus];
    }

    /**
     * @param Virus $virus
     * @return bool
     */
    private function hasVirus(Virus $virus): bool
    {
        return array_key_exists((string)$virus, $this->contaminated);
    }
}
