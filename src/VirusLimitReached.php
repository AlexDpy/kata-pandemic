<?php

namespace Kata;

use League\Event\Event;

class VirusLimitReached extends Event
{
    const NAME = 'virus_limit_reached';

    /**
     * @var City
     */
    private $city;

    /**
     * @var Virus
     */
    private $virus;

    /**
     * VirusLimitReached constructor.
     *
     * @param City  $city
     * @param Virus $virus
     */
    public function __construct(City $city, Virus $virus)
    {
        parent::__construct(self::NAME);

        $this->city = $city;
        $this->virus = $virus;
    }

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @return Virus
     */
    public function getVirus(): Virus
    {
        return $this->virus;
    }
}
