<?php

namespace Kata;

class Eclosion
{
    /**
     * @var City
     */
    private $city;

    /**
     * @var Virus
     */
    private $virus;

    /**
     * Eclosion constructor.
     *
     * @param City  $city
     * @param Virus $virus
     */
    public function __construct($city, $virus)
    {
        $this->city = $city;
        $this->virus = $virus;
    }

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @return Virus
     */
    public function getVirus(): Virus
    {
        return $this->virus;
    }
}
