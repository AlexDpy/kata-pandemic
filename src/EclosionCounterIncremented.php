<?php

namespace Kata;

use League\Event\Event;

class EclosionCounterIncremented extends Event
{
    /**
     * @var string
     */
    const NAME = 'eclosion-counter-incremented';

    /**
     * @var int
     */
    private $count;

    /**
     * EclosionCounterIncremented constructor.
     *
     * @param int $count
     */
    public function __construct(int $count)
    {
        parent::__construct(self::NAME);

        $this->count = $count;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->count;
    }
}
