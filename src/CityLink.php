<?php

namespace Kata;

class CityLink
{

    /**
     * @var City
     */
    private $city1;

    /**
     * @var City
     */
    private $city2;

    /**
     * CityLink constructor.
     *
     * @param City $city1
     * @param City $city2
     */
    public function __construct(City $city1, City $city2)
    {
        $this->city1 = $city1;
        $this->city2 = $city2;
    }

    /**
     * @param City $city
     * @return bool
     */
    public function contains(City $city): bool
    {
        return $this->city1 === $city || $this->city2 === $city;
    }

    /**
     * @param City $city
     *
     * @return City
     *
     * @throws \InvalidArgumentException
     */
    public function getOtherCity(City $city): City
    {
        if (!$this->contains($city)) {
            throw new \InvalidArgumentException();
        }

        return $this->city1 === $city ? $this->city2 : $this->city1;
    }
}
