<?php

namespace Kata;

use League\Event\Event;

class EclosionCounterLimitReached extends Event
{
    /**
     * @var string
     */
    const NAME = 'eclosion-counter-limit-reached';

    /**
     * GameLost constructor.
     */
    public function __construct()
    {
        parent::__construct(self::NAME);
    }
}
