<?php

namespace Kata;

final class Virus
{
    const RED = 'red';
    const BLUE = 'blue';
    const YELLOW = 'yellow';
    const BLACK = 'black';

    /**
     * @var string
     */
    private $color;

    private function __construct(string $color)
    {
        $this->color = $color;
    }

    /**
     * @return Virus
     */
    public static function red(): Virus
    {
        return new self(self::RED);
    }

    public static function blue(): Virus
    {
        return new self(self::BLUE);
    }

    public static function black(): Virus
    {
        return new self(self::BLACK);
    }

    public static function yellow(): Virus
    {
        return new self(self::YELLOW);
    }

    public function __toString(): string
    {
        return $this->color;
    }
}
