<?php

namespace Test\Kata;

use Kata\City;
use Kata\CityLink;
use Kata\Eclosion;
use Kata\Listeners\MapEclosionListener;
use Kata\Map;
use Kata\Virus;
use Kata\VirusLimitReached;
use League\Event\Emitter;
use League\Event\EmitterInterface;
use PHPUnit\Framework\TestCase;

class MapTest extends TestCase
{
    /**
     * @var Map
     */
    private $map;

    /**
     * @var City
     */
    private $atlanta;

    /**
     * @var City
     */
    private $washington;

    /**
     * @var City
     */
    private $chicago;

    /**
     * @var City
     */
    private $montreal;

    /**
     * @var Eclosion
     */
    private $eclosion;

    /**
     * @var Virus
     */
    private $virus;

    /**
     * @var EmitterInterface
     */
    private $eventEmitter;

    protected function setUp()
    {
        $this->eventEmitter = new Emitter();
    }

    protected function tearDown()
    {
        $this->atlanta = null;
        $this->washington = null;
        $this->chicago = null;
        $this->montreal = null;
        $this->map = null;
    }

    public function testItShouldGiveTheNeighbourCities()
    {
        $this->givenAtlanta();
        $this->givenAMap();

        static::assertEquals(
            [
                $this->washington,
                $this->chicago,
            ],
            $this->map->getNeighbours($this->atlanta)
        );
    }

    public function testItShouldContaminateAtlanta()
    {
        $this->givenAtlanta();
        $this->givenAMap();
        $this->givenAVirus();
        $this->givenAnEclosion($this->atlanta, $this->virus);

        static::assertEquals(1, $this->chicago->countContamination($this->virus));
        static::assertEquals(1, $this->washington->countContamination($this->virus));
        static::assertEquals(0, $this->montreal->countContamination($this->virus));
    }

    private function givenAMap()
    {
        $this->washington = new City('washington', $this->eventEmitter);
        $this->montreal = new City('montreal', $this->eventEmitter);
        $this->chicago = new City('chicago', $this->eventEmitter);

        $links = [
            new CityLink($this->atlanta, $this->washington),
            new CityLink($this->atlanta, $this->chicago),
            new CityLink($this->chicago, $this->montreal),
            new CityLink($this->montreal, $this->washington),
        ];

        $this->map = new Map($links);

        $this->eventEmitter->addListener(VirusLimitReached::NAME, new MapEclosionListener($this->map));
    }

    private function givenAtlanta()
    {
        $this->atlanta = new City('atlanta', $this->eventEmitter);
    }

    private function givenAVirus()
    {
        $this->virus = Virus::black();
    }

    private function givenAnEclosion($atlanta, $virus)
    {
        $this->atlanta->contaminate($virus);
        $this->atlanta->contaminate($virus);
        $this->atlanta->contaminate($virus);
        $this->atlanta->contaminate($virus);
    }
}
