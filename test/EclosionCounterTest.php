<?php

namespace Test\Kata;

use Kata\City;
use Kata\Eclosion;
use Kata\EclosionCounter;
use Kata\EclosionCounterLimitReached;
use Kata\Listeners\CounterEclosionListener;
use Kata\Virus;
use Kata\VirusLimitReached;
use League\Event\Emitter;
use League\Event\EmitterInterface;
use League\Event\EventInterface;
use League\Event\ListenerInterface;
use PHPUnit\Framework\TestCase;

class EclosionCounterTest extends TestCase
{
    /**
     * @var bool
     */
    public static $lost;

    /**
     * @var EclosionCounter
     */
    private $counter;

    /**
     * @var EmitterInterface
     */
    private $emitter;

    /**
     * @var Eclosion
     */
    private $virusLimitReached;

    /**
     * Init the mocks
     */
    public function setUp()
    {
        $this->emitter = new Emitter();
        self::$lost = false;
    }

    /**
     * Closes the mocks
     */
    public function tearDown()
    {
        \Mockery::close();
    }

    /**
     * @test
     */
    public function itShouldCountEclosions()
    {
        $this->givenAnEclosionCounter();
        $this->givenAVirusLimitReached();

        self::assertEquals(1, $this->counter->count());
    }

    /**
     * @test
     */
    public function itShouldEndTheGameAfterThe8thEclosion()
    {
        $this->givenAnEclosionCounter();
        $this->givenAnEndGameDetector();

        $this->givenAVirusLimitReached();
        $this->givenAVirusLimitReached();
        $this->givenAVirusLimitReached();
        $this->givenAVirusLimitReached();
        $this->givenAVirusLimitReached();
        $this->givenAVirusLimitReached();
        $this->givenAVirusLimitReached();

        static::assertFalse(self::$lost);

        $this->givenAVirusLimitReached();

        static::assertTrue(self::$lost);
    }

    private function givenAnEclosionCounter()
    {
        $this->counter = new EclosionCounter($this->emitter);
        $this->emitter->addListener(VirusLimitReached::NAME, new CounterEclosionListener($this->counter));
    }

    private function givenAVirusLimitReached()
    {
        $this->virusLimitReached = new VirusLimitReached(new City('Atlanta', $this->emitter), Virus::black());
        $this->emitter->emit($this->virusLimitReached);
    }

    private function givenAnEndGameDetector()
    {
        $this->emitter->addListener(
            EclosionCounterLimitReached::NAME,
            new class implements ListenerInterface
            {
                /**
                 * Handle an event.
                 *
                 * @param EventInterface $event
                 *
                 * @return void
                 */
                public function handle(EventInterface $event)
                {
                    EclosionCounterTest::$lost = true;
                }

                /**
                 * Check whether the listener is the given parameter.
                 *
                 * @param mixed $listener
                 *
                 * @return bool
                 */
                public function isListener($listener)
                {
                    return $listener === $this;
                }
            }
        );
    }
}
