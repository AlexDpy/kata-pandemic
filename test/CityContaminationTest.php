<?php

namespace Test\Kata;

use Kata\City;
use Kata\Virus;
use League\Event\Emitter;
use League\Event\EmitterInterface;
use PHPUnit\Framework\TestCase;

class CityContaminationTest extends TestCase
{
    /**
     * @var City
     */
    private $city;

    /**
     * @var EmitterInterface
     */
    private $eventEmitter;

    protected function setUp()
    {
        $this->eventEmitter = new Emitter();
    }


    protected function tearDown()
    {
        $this->city = null;
    }

    public function testItShouldContaminateACityWith2RedViruses()
    {
        $virus = Virus::red();

        $this->givenWeHaveANonContaminatedCity();
        $this->whenCityIsContaminated($virus, 2);

        static::assertEquals(2, $this->city->countContamination($virus));
        static::assertEquals(0, $this->city->countContamination(Virus::blue()));
        static::assertEquals(0, $this->city->countContamination(Virus::yellow()));
        static::assertEquals(0, $this->city->countContamination(Virus::black()));
    }

    public function testItShouldContaminateACityWith1OfEachVirus()
    {
        $red = Virus::red();
        $blue = Virus::blue();
        $yellow = Virus::yellow();
        $black = Virus::black();

        $this->givenWeHaveANonContaminatedCity();
        $this->whenCityIsContaminated($red, 1);
        $this->whenCityIsContaminated($blue, 1);
        $this->whenCityIsContaminated($yellow, 1);
        $this->whenCityIsContaminated($black, 1);

        static::assertEquals(1, $this->city->countContamination($red));
        static::assertEquals(1, $this->city->countContamination($blue));
        static::assertEquals(1, $this->city->countContamination($yellow));
        static::assertEquals(1, $this->city->countContamination($black));
    }

    public function testACityShouldNotBeContaminatedMoreThan3TimesWithTheSameVirus()
    {
        $red = Virus::red();

        $this->givenWeHaveANonContaminatedCity();
        $this->whenCityIsContaminated($red, 4);

        static::assertEquals(3, $this->city->countContamination($red));
    }




    private function givenWeHaveANonContaminatedCity()
    {
        $this->city = new City('atlanta', $this->eventEmitter);
    }

    private function whenCityIsContaminated(Virus $virus, $count)
    {
        for ($i = 0; $i < $count; ++$i) {
            $this->city->contaminate($virus);
        }
    }
}
